variable "vm_liste" {
  default = ["projet-devops-1","projet-devops-2"]
}

variable "aws_region" {
  type = string
}

variable "aws_access_key" {
  type = string
}

variable "aws_secret_access_key" {
  type = string
}
