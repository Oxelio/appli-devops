terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.74.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_access_key
}

resource "aws_security_group" "instance_ec2" {
    name="projet"
    tags = {
        Name = "projet"
    }

    # Port http
    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = [ "0.0.0.0/0" ]
    }

    # Port ssh
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [ "0.0.0.0/0" ]
    }

    # Tout ouvrir en sortant
    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
}

resource "aws_key_pair" "instance_ec2" {
  key_name = "projet-devops.key"
  public_key = file("./Key_SSH/id.pub")
}

resource "aws_instance" "vm-stephen" {
    count = length(var.vm_liste)
    tags = {
      Name = "var-${var.vm_liste[count.index]}"
    }
    ami = "ami-050949f5d3aede071" # Debian 10
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instance_ec2.id]
    key_name = aws_key_pair.instance_ec2.key_name

    connection {
      timeout = "30s"
      type = "ssh"
      user = "admin"
      private_key = file("./Key_SSH/id")
      host = self.public_ip
    }

    provisioner "file" {
      source      = "../docker-compose_prod.yml"
      destination = "/home/admin/docker-compose.yml"
    }

    provisioner "file" {
      source      = "../Nginx"
      destination = "/home/admin"
    }

    provisioner "remote-exec" {
      scripts =[
        "./Scripts/install_docker.sh",
        "./Scripts/install.sh"
      ]
    }
}
