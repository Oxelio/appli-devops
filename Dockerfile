FROM python:3.10.2

WORKDIR /App

ENV FLASK_APP=routes

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000

CMD ["bash","script.sh"]
