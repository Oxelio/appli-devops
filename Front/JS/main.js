// -------------------------
//
// Fonction de la map
//
// -------------------------

$.ajaxSetup({
  headers: { 'Access-Control-Allow-Origin': '*' }
});

function mapTrajet() {
  type = $("#type_etablissement").val()
  let request = $.ajax({
    type: "GET",
    url: `http://localhost/api/v1/map/trajet?type=${type}`,
    dataType: "html",
    beforeSend: function () {},
  });

  request.done(function (response) {
    $(".etablissement-content").html(response);
    $(".ajout-form").hide();
    $(".etablissement-content").show();
  });

  request.fail(function (error) {
    console.log(error)
  });

  request.always(function () {});
}

// ------------------------------------
//
// Fonction pour les établissements
//
// ------------------------------------

function afficheEtablissement(id) {
  let request = $.ajax({
    type: "GET",
    url: `http://localhost/api/v1/etablissement?id=${id}`,
    beforeSend: function () {
    },
  });
  request.done(function (res) {
    // On recoit une liste de 1 élément et on veut le premier élément
    response = res[0]
    html = ` <div class="p-4 col-md-10 offset-md-1">
              <h3 class="p-4 text-center">${response.nom}</h3>
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th scope="row">Id</th>
                    <td>${response.id}</td>
                  </tr>
                  <tr>
                    <th scope="row">Nom</th>
                    <td>${response.nom}</td>
                  </tr>
                  <tr>
                    <th scope="row">Type d'établissement</th>
                    <td>${response.type}</td>
                  </tr>
                  <tr>
                    <th scope="row">Mail</th>
                    <td>${response.mail}</td>
                  </tr>
                  <tr>
                    <th scope="row">Tel</th>
                    <td>${response.tel}</td>
                  </tr>
                  <tr>
                    <th scope="row">Adresse</th>
                    <td>${response.adresse}</td>
                  </tr>
                  <tr>
                    <th scope="row">GPS</th>
                    <td>Latitude: ${response.geolat},  Longitude: ${response.geolng}</td>
                  </tr>
                  <tr>
                    <th scope="row">Horaire</th>
                    <td>${response.horaire}</td>
                  </tr>
                  <tr>
                    <th scope="row">Description</th>
                    <td>${response.description}</td>
                  </tr>
                </tbody>
              </table>
              <button type="button" class="btn btn-primary px-4" onclick="cacheEtablissement()">  Cacher Etablissement  </button>
            </div>
          `;
          test = ".numProfil"+id
          $(".affichageProfil").hide();
          $(test).html(html);
          $(test).show();
  });
  request.fail(function (error) {
  });
  request.always(function () {
  });
}

// Fonction retournant tout nos établissements dans notre base de donnée
function etablissement() {
  let request = $.ajax({
    type: "GET",
    url: "http://localhost/api/v1/etablissement",
    dataType: "json",
    beforeSend: function () {},
  });

  request.done(function (response) {
    $(".accueil").hide();
    // Entête de notre tableau pour afficher la liste des établissements
    let html = `
                <div class="p-4">
                  <h3 class="p-4 text-center">Liste des établissements</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <th scope="col">Nom</th>
                            <th scope="col">Type</th>
                            <th scope="col">Mail</th>
                            <th scope="col">Téléphone</th>
                            <th scope="col">Adresse</th>
                            <th scope="col">Horaire</th>
                            <th scope="col">Description</th>
                            <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                          `;
    // On récupère les informations de notre base de donnée et on affiche autant qu'il y en a
    response.reverse().map((etablissement) => {
      html += `<tr>
                <th scope="row">${etablissement.nom}</th>
                <td>${etablissement.type}</td>
                <td>${etablissement.mail}</td>
                <td>${etablissement.tel}</td>
                <td>${etablissement.adresse}</td>
                <td>${etablissement.horaire}</td>
                <td>${etablissement.description}</td>
                <td>
                    <button type="button" class="btn btn-primary px-4" onclick="afficheEtablissement(${etablissement.id})">  Voir  </button>
                    <button type="button" class="btn btn-info px-3" onclick="modifEtablissement(${etablissement.id})">  Modifier  </button>
                    <button type="button" class="btn btn-danger" onclick="suppEtablissement(${etablissement.id})">Supprimer</button>
                </td>
                </tr>
                <tr>
                   <td style="display: none;" colspan="7" class="affichageProfil numProfil${etablissement.id}">
                   </td>
               </tr>
              `;
    });
    // Fin de notre tableau
    html += `
              </tbody>
            </table>
          </div>
        `;

  // On affiche
  $(".etablissement-content").html(html);
  $(".ajout-form").hide();
  $(".etablissement-content").show();

  // On change le bouton de navigation
  $(".active").removeClass("bg-secondary")
  $(".users").addClass("bg-secondary")
  $(".affich-form").removeClass("bg-secondary");

});

request.fail(function (error) {
  console.log(error);
});

request.always(function () {});
}

function ajoutEtablissement() {
  // let mailformat = /^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,3})+$/;
  // let telformat = /^((+)33|0)[1-9](\d{2}){4}$/;
  // let condition = ($("#nom").val() == "" ) || ($("#mail").val() == "") || $("#tel") == "" || $("#adresse").val() == "" || $("#horaire").val() == "" || $("#description").val() == "" ||  !($("#mail").val().match(mailformat)) || !($("#tel").val().match(telformat));

  let condition = ($("#nom").val() == ""  || $("#mail").val() == "" || $("#tel") == "" || $("#adresse").val() == "" || $("#horaire").val() == "" || $("#description").val() == "");

  if (condition)
  {
    $(".sucessMsg").text("Veuillez remplir tout les champs");
    $(".sucessMsg").show()
    $(".sucessMsg").removeClass("alert-success")
    $(".sucessMsg").addClass("alert-danger")

    setTimeout(() => {
      $(".sucessMsg").text("");
      $(".sucessMsg").hide();
    }, 5000);
  }
  else {
    let request = $.ajax({
      type: "POST",
      url: "http://localhost/api/v1/etablissement",
      dataType: "json",
      data: {
        nom: $("#nom").val(),
        type_etablissement: $("#type_etablissement_form").val(),
        mail: $("#mail").val(),
        tel: $("#tel").val(),
        adresse: $("#adresse").val(),
        horaire: $("#horaire").val(),
        description: $("#description").val()
      },
      beforeSend: function () {},
    });
    request.done(function (reponse) {
      $(".sucessMsg").text("Ajout d'un établissement avec succès");
      $(".sucessMsg").show()
      $(".sucessMsg").removeClass("alert-danger")
      $(".sucessMsg").addClass("alert-success")
      resetForm();
      setTimeout(() => {
          $(".sucessMsg").text("");
          $(".sucessMsg").hide();
        }, 5000);
    });
    request.fail(function (error) {
      console.log(error);
    });
    request.always(function () {});
  }

}

function modifEtablissement(id) {
  let request = $.ajax({
    type: "GET", //Méthode à employer
    url: `http://localhost/api/v1/etablissement?id=${id}`, //Cible du script coté serveur à appeler
    beforeSend: function () {
    },
  });
  request.done(function (rep) {
    response = rep[0]
    $("#old_id").val(response.id);
    $("#type_etablissement").val(response.type);
    $("#nom").val(response.nom);
    $("#mail").val(response.mail);
    $("#tel").val(response.tel);
    $("#adresse").val(response.adresse);
    $("#horaire").val(response.horaire);
    $("#description").val(response.description);

    $(".ajout-form").show();
    $(".ajout-btn").hide();
    $(".sucessMsg").hide();
    $(".maj-btn").show();
    $(".form-titre").text("Modification de l'établissement");
    $(".etablissement-content").hide();
  });
  request.fail(function (error) {
    //Code à jouer en cas d'éxécution en erreur du script du PHP ou de ressource introuvable
  });
  request.always(function () {
    //Code à jouer après done OU fail quoi qu'il arrive
  });
}

function majEtablissement() {
  let request = $.ajax({
    type: "PUT",
    url: `http://localhost/api/v1/etablissement`, // Chemin de notre API
    dataType: "json",
    data: {
      id: $("#old_id").val(),
      nom: $("#nom").val(),
      type_etablissement: $("#type_etablissement_form").val(),
      mail: $("#mail").val(),
      tel: $("#tel").val(),
      adresse: $("#adresse").val(),
      horaire: $("#horaire").val(),
      description: $("#description").val()
    },
    beforeSend: function () {
      //Code à appeler avant l'appel ajax en lui même
    },
  });
  request.done(function (response) {
    $(".ajout-form").hide();
    etablissement();
    $(".etablissement-content").show();
  });
  request.fail(function (error) {
    //Code à jouer en cas d'éxécution en erreur du script du PHP ou de ressource introuvable
  });
  request.always(function () {
    //Code à jouer après done OU fail quoi qu'il arrive
  });
}

function suppEtablissement(id){
  let request = $.ajax({
      type: "DELETE",
      url: `http://localhost/api/v1/etablissement?id=${id}`,
      beforeSend: function () {
      },
    });
    request.done(function (response) {
      etablissement()
    });
    request.fail(function (error) {
    });
    request.always(function () {
    });
}
function cacheEtablissement(){
  $(".affichageProfil").hide();
}

// ------------------------------------
//
//  Accueil
//
// ------------------------------------

function accueil(){
  $(".active").addClass("bg-secondary")
  $(".users").removeClass("bg-secondary")
  $(".affich-form").removeClass("bg-secondary");

  $(".etablissement-content").hide();
  $(".ajout-form").hide();
  $(".accueil").show();
}

function afficheFormulaire() {
  $(".etablissement-content").hide();
  $(".ajout-form").show();
  $(".sucessMsg").hide();
  $(".accueil").hide();
  $(".form-titre").text("Ajout d'un établissement");
  $(".ajout-btn").show();
  $(".maj-btn").hide();
  resetForm();

  $(".active").removeClass("bg-secondary");
  $(".users").removeClass("bg-secondary");
  $(".affich-form").addClass("bg-secondary");
}

function resetForm() {
  $("#old_id").val("");
  $("#type_etablissement").val("");
  $("#nom").val("");
  $("#mail").val("");
  $("#tel").val("");
  $("#adresse").val("");
  $("#horaire").val("");
  $("#description").val("");
}
