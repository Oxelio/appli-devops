from flask import Flask
from flask_cors import CORS
from flask_mysqldb import MySQL

app = Flask(__name__, template_folder='../../Front/templates')
CORS(app)
mysql = MySQL(app)

app.config['MYSQL_HOST']='sql'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='root'
app.config['MYSQL_DB']='bd'
app.config['MYSQL_PORT']=3306

from Api import routes
