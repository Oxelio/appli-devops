from flask import jsonify, Response
import codecs
from pathlib import Path
from Api import mysql
from geopy.geocoders import Nominatim

class ApiController:
  def index(self):
    path1 = Path(__file__).parent # Api
    path2 = Path(path1).parent # Back
    path = str(Path(path2).parent) + "/Front/index.html" # Application
    file = codecs.open(path, 'r', "utf-8")
    # Retourne notre fichier front index.html
    return (file.read())

# GET

  def get_css(self, name):
    # ../Api
    path1 = Path(__file__).parent
    # ../Back
    path2 = Path(path1).parent
    # /Application/Front/CSS/{name of the file}
    chemin = str(Path(path2).parent) + "/Front/CSS/"+ name
    file = codecs.open(chemin, 'r', "utf-8")

    return Response( file.read(),mimetype="text/css")

  def get_js(self, name):
    path1 = Path(__file__).parent
    path2 = Path(path1).parent
    chemin = str(Path(path2).parent) + "/Front/JS/"+ name
    file = codecs.open(chemin, 'r', "utf-8")

    return Response( file.read(),mimetype="text/javascript")

  def get_one_etablissement(self, identifiant):
    """
    Récuper un établissement par rapport à son id

    Args:
        identifiant (int): numéro d'identifiant de l'établissement

    Returns:
        (json): la réponse sous format json
    """
    try :
      cur = mysql.connection.cursor()
      cur.execute("SELECT * FROM etablissement WHERE ID_ETABLISSEMENT={}".format(identifiant))
      resp = cur.fetchall()
      cur.close()

      return jsonify(self.info_etablissement(resp)), 200
    except Exception as e:
      return print(e), 400

  def get_all_etablissement(self):
    """
    Récuper tous les établissements

    Returns:
        (json): Renvoie tout nos établissements sous format json
    """
    try :
      cur = mysql.connection.cursor()
      cur.execute("SELECT * FROM etablissement")
      resp = cur.fetchall()
      cur.close()

      return jsonify(self.info_etablissement(resp)), 200
    except Exception as e:
      return jsonify({"Error":e}), 400

  def get_etablissement_type(self, type):
    try :
      cur = mysql.connection.cursor()
      cur.execute("SELECT * FROM etablissement WHERE TYPE_ETABLISSEMENT = '{}' ".format(type))
      resp = cur.fetchall()
      cur.close()

      return jsonify(self.info_etablissement(resp)), 200
    except Exception as e:
      return jsonify({"Error":e}), 400

# POST

  def post_etablissement(self,nom,type_etablissement,mail,tel,adresse,horaire,description):
    """
    Ajoute un établissement

    Args:
        nom (string): Nom de l'établissement
        mail (string): Mail de l'établissement
        tel (int): Numéro de téléphone
        adresse (string): Adresse de la rue
        horaire (string): Horaire
        description (string): Description

    Returns:
        (json): Renvoie une réponse de réussite ou d'échec
    """
    try:
      cur = mysql.connection.cursor()
      geo = self.adresse_to_geo(str(adresse))
      geo1 = float(geo[0])
      geo2 = float(geo[1])
      text = "INSERT INTO etablissement(NOM_ETABLISSEMENT,TYPE_ETABLISSEMENT,MAIL,TEL,ADRESSE,GEOLAT,GEOLNG,HORAIRE,DESCRIPTION) VALUES ('{}','{}','{}','{}','{}',{},{},'{}','{}')".format(nom, type_etablissement, mail,tel, adresse, geo1, geo2, horaire, description)

      cur.execute(text)
      mysql.connection.commit()
      cur.close()

      return jsonify({"POST Etablissement":"Reussie"}), 200

    except Exception as e:
      return jsonify({"Error":e}), 400

# PUT

  def put_etablissement(self,identifiant,nom,type_etablissement,mail,tel,adresse,horaire,description):
    try:
      cur = mysql.connection.cursor()
      geo = self.adresse_to_geo(str(adresse))
      geo1 = float(geo[0])
      geo2 = float(geo[1])
      text = "UPDATE etablissement SET NOM_ETABLISSEMENT='{}', TYPE_ETABLISSEMENT='{}', MAIL='{}',TEL='{}',ADRESSE='{}',GEOLAT={},GEOLNG={},HORAIRE='{}',DESCRIPTION='{}' WHERE ID_ETABLISSEMENT={}".format(nom, type_etablissement, mail,tel, adresse, geo1, geo2, horaire, description,identifiant)
      cur.execute(text)
      mysql.connection.commit()
      cur.close()
      return jsonify({"PUT Etablissement":"Reussie"}), 200

    except Exception as e:
      return jsonify({"Error":e}), 400

# DELETE

  def delete_etablissement(self, identifiant):
    try:
      cur = mysql.connection.cursor()
      cur.execute("DELETE FROM etablissement WHERE ID_ETABLISSEMENT={}".format(identifiant))
      mysql.connection.commit()
      cur.close()
      return jsonify({"DELETE Etablissement":"Reussie"}), 200
    except Exception as e:
      return jsonify({"Error":e}), 400

# FONCTION PRATIQUE

  def adresse_to_geo(self, adresse):
    """
    Transforme une adresse en position gps

    Args:
      adresse (string): adresse

    Renvoie:
      (tuple): coordonée gps
    """
    geolocator = Nominatim(user_agent= "my_appli")
    location = geolocator.geocode(adresse)
    return(location.latitude, location.longitude)

  def info_etablissement(self, resp):
    reponse = []
    if resp is not None:
      for ligne in resp:
        reponse.append({
          'id': ligne[0],
          'nom': ligne[1],
          'type': ligne[2],
          'mail': ligne[3],
          'tel': ligne[4],
          'adresse': ligne[5],
          'geolat': ligne[6],
          'geolng': ligne[7],
          'horaire': ligne[8],
          'description': ligne[9]
        })
    return reponse

