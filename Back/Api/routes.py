from Api import app
from flask import request, jsonify, render_template
import openrouteservice as ors
import folium
import geocoder

from Api.controllerApi import ApiController

@app.route("/")
def home():
  return render_template("home.html")

@app.route("/v1")
def index():
  """
  Accueil de notre Application
    ---
    responses:
      HTML : Retourn notre front index.html
  """
  return ApiController().index()

@app.route("/poulet")
def hello_poulet():
  """
  Route easter egg
    ---
    responses:
      200:
        description: 'Hello poulet'
  """
  return "<h1>Hello Poulet</h1>"

@app.route('/api/v1/map/trajet', methods=['GET'])
def trajet():
  try:
    type_etabli = str(request.args['type'])
  except Exception as e:
    return jsonify({"error of type": "Fail","error":e})

  # Se connect au compte OpenRouteService
  client = ors.Client(key='5b3ce3597851110001cf62482654621efdec4e1b8645c4fdb2754159')

  # Coordonée de l'utilisateur

  g = geocoder.ip('me')
  start_coords = (g.latlng)

  # Création de la carte
  folium_map = folium.Map(location=start_coords, zoom_start=14)

  # Coordonné de trajet
  coordinates = list()
  # Controleur qui permet de récuper les établissements selon le type
  type_etablissement = ApiController().get_etablissement_type(type_etabli)[0]

  data = type_etablissement.get_json()

  # Petit Popup
  try:
    for etabli in data:
      coord = []
      coord.append(etabli["geolng"])
      coord.append(etabli["geolat"])
      coordinates.append(coord)

      folium.Marker(location=list(reversed(coord)),
                    popup=folium.Popup("Nom: {}\nDecription : {}".format(etabli["nom"],etabli["description"]))).add_to(folium_map)
  except Exception as e:
    return jsonify({"Popup": "Fail","error":e})

  # Connexion API OpenRouteService
  try:
    route = client.directions(
        coordinates=coordinates,
        profile='foot-walking',
        format='geojson',
        validate=False,
        optimize_waypoints=True
    )
  except Exception as e:
    return jsonify({"Connexion API OpenRouteService": "Fail","error":e})

  # Dessin de la route
  try:
    folium.PolyLine(locations=[list(reversed(coord))
                            for coord in
                            route['features'][0]['geometry']['coordinates']]).add_to(folium_map)
  except Exception as e:
    return jsonify({"Dessin chemin": "Fail","error":e})

  return folium_map._repr_html_(), 200

@app.route("/api/v1/etablissement", methods=['GET','POST','PUT','DELETE'])
def etablissement():
  """
    Route pour les données de nos établissements

    Renvoie:
      (json): informations
  """
  # Méthode GET
  if request.method=='GET':
    # Si on a un id dans la requete on selectionne l'etablissement correspondant
    if 'id' in request.args:
      identifiant = int(request.args['id'])
      # On demande à notre controller de renvoyer l'établissement correspondant
      return ApiController().get_one_etablissement(identifiant)
    # Sinon on renvoie par défaut tout les établissements
    else:
      # On demande à notre controller de renvoyer tous les établissements
      return ApiController().get_all_etablissement()

  # Méthode POST
  elif request.method=='POST':
    # On récupère les données envoyé par le front
    nom = request.form['nom']
    type_etablissement = request.form['type_etablissement']
    mail = request.form['mail']
    tel = request.form['tel']
    adresse = request.form['adresse']
    horaire = request.form['horaire']
    description = request.form['description']

    # On demande au controler de rajouter l'établissement
    return ApiController().post_etablissement(nom,type_etablissement,mail,tel,adresse,horaire,description)

  # Méthode PUT
  elif request.method == 'PUT':
    # On récupère les données envoyé par le front
    identifiant = int(request.form['id'])
    nom = request.form['nom']
    type_etablissement = request.form['type_etablissement']
    mail = request.form['mail']
    tel = request.form['tel']
    adresse = request.form['adresse']
    horaire = request.form['horaire']
    description = request.form['description']

    # On demande au controler de modifer l'établissement choisi
    return ApiController().put_etablissement(identifiant,nom,type_etablissement,mail,tel,adresse,horaire,description)

  # Méthode DELETE
  elif request.method=='DELETE' :
    # On vérifie qu'on a bien un id dans la requête
    if 'id' in request.args:
      # Si oui on le récupère
      identifiant = int(request.args['id'])
      # On demande au controler de supprimer l'établissement
      return ApiController().delete_etablissement(identifiant)
    else:
      # Si on a pas l'argument id dans la requête, on renvoie une réponse
      return jsonify({"DELETE api/v1/etablissement":"Oublie d'un argument : api/v1/etablissement?id=.."}), 400

  else:
    # Si on arrive ici c'est que l'utilisateur utilise une méthode que nous ne supportons pas, on lui renvoie une réponse
    return jsonify({"API route : api/v1/etablissement":"Echec la méthode n'exite pas"}), 405

@app.route('/CSS/<string:name>', methods=['GET'])
def get_css(name):
  """
  Fichier CSS

  Paramètre : nom de la ressource CSS à chercher

  Returns:
      text/css: Retourne le CSS
  """
  # On demande au controler de nous donner le CSS voulu
  return ApiController().get_css(name)

@app.route('/JS/<string:name>', methods=['GET'])
def get_js(name):
  """
  Fichier JS

  Paramètre : nom de la ressource JS à chercher

  Returns:
      text/css: Retourne le JS
  """
  # On demande au controler de nous donner le JS voulu
  return ApiController().get_js(name)

